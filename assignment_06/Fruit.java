package cs102.week08;

public abstract class Fruit {
    String color;
    public abstract String getVitamin();
}
