package cs102.week08;

public class Apple extends TreeFruit {
    public Apple() {
        this.color = "green";
    }

    public String getVitamin() {
        return "A B12";
    }

    public void peel() {
        System.out.println("Peeling an apple.");
    }
}
