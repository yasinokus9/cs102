package cs102.week07;

public class Soap extends Cleaning {
    private int quantity;

    public Soap(String brand, int quantity) {
        super(brand);
        this.quantity = quantity;
    }

    public double calculateTotalPrice() {
        return (getPrice() * (getTaxRate() + 100) / 100) * this.quantity;
    }
}
