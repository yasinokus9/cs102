package cs102.week03;

public class DriverLicense {
    private String driverName;
    private String driverID;
    private String expirationDate;
    private String bloodType;
    private int penalty;
    private boolean status;

    public DriverLicense(String dn, String di, String ed, String bt){
        driverName = dn;
        driverID = di;
        expirationDate = ed;
        bloodType = bt;
        penalty = 0;
        status = true;
    }

    public void increasePenalty(int penaltyPoints){
        penalty += penaltyPoints;
        setStatus();
    }

    public void decreasePenalty(int penaltyPoints){
        if(penalty - penaltyPoints > 0){
            penalty -= penaltyPoints;
        }
        else{
            penalty = 0;
        }
        setStatus();
    }

    private void setStatus(){
        if(penalty >= 100){
            status = false;
        }
        else if(!status && penalty < 20){
            status = true;
        }
    }

    public String toString(){
        return " Name : " + driverName +
                "\n Id : " + driverID +
                "\n Expiration Date : " + expirationDate +
                "\n Blood Type : "  + bloodType +
                "\n Penalty : " + penalty +
                "\n Status : " + status +
                "\n -------------";
    }
}
